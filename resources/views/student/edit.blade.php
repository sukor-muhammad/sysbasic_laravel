
@extends('layouts.app')


@section('content')


<form action="{{ route('student.update',$student) }}" method="POST">
    @csrf
    @method('put')

    @include('student._form')


    
    <button type="submit" class="btn btn-primary">Submit</button>

  </form>

  @endsection