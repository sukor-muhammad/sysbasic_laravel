
@extends('layouts.app')


@section('content')


<form action="{{ route('student.store') }}" method="POST">
    @csrf


    @include('student._form')


    
    <button type="submit" class="btn btn-primary">Submit</button>

  </form>

  @endsection