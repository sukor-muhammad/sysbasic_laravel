<div class="mb-3">
    <label for="student_name" class="form-label">student_name</label>
    <input value='{{old('student_name')??($student->student_name??'')}}' type="text" name='student_name' class="form-control" id="student_name" aria-describedby="emailHelp">
    
  </div>
  <div class="mb-3">
    <label for="student_mykad" class="form-label">student_mykad</label>
    <input value='' type="text"  name='student_mykad' class="form-control @error('student_mykad') is-invalid @enderror " id="student_mykad" >
    @error('student_mykad')
    <span class="invalid-feedback" role="alert">
     {{$message}}
    </span>
    @enderror


  </div>

  <div class="mb-3">
    <label for="student_gender" class="form-label">student_gender</label>

    <select id='student_gender' name='student_gender' class="form-select  @error('student_gender') is-invalid @enderror" aria-label="Default select example">
        <option selected>Open this select menu</option>
        @foreach ($gender as $key=>$item)
        <option {{(($student->student_gender??'')==$key?'selected':'')}}     value="{{$key}}">{{$item}}</option>
        @endforeach
        
    
        
      </select>
   
    @error('student_gender')
    <span class="invalid-feedback" role="alert">
     {{$message}}
    </span>
    @enderror


  </div>



  <div id='colordiv' class="mb-3">

    <label for="color" class="form-label">color</label>
    <input value='' type="text"  name='color' class="form-control @error('color') is-invalid @enderror " id="color" >
    @error('color')
    <span class="invalid-feedback" role="alert">
     {{$message}}
    </span>
    @enderror


  </div>




  @section('footer-scripts')

<script>
  $(document).ready(function(){
    $('#colordiv').hide();

  var genderdefault=  $(student_gender).val();


  hidecollor(genderdefault);





  
    $('#student_gender').on("change",
     function(){

  
     var gender= $(this).val();
     
  hidecollor(gender);


    });

    

    

  }); // end of docreadt
  

  function hidecollor(gender){

  
    if(gender=='M'){

$('#colordiv').hide();

}else if(gender=='F'){

$('#colordiv').show();
}
  }
  
  

</script>



  @endsection