<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        

        // ->sortable()


      $d['student']=  Student::filter(request(['student_name', 'student_mykad']))->paginate(10);

      
        //
        return view('student.index',$d);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       $student= Student::find($id);

    //    $student=Student::where(['student_gender'=>'M'])->get();

        dd($student);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $d['student']= Student::find($id);
        // dd($student);

        return view('student.edit',$d);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,student $student)
    {
        $validate = $request->validate([
            'student_name' => 'required',
            'student_mykad' => 'required',
        ],[
            'student_name.required' => 'Nama Adalah Diperlukan',
            'student_mykad.required' => 'student_mykad Adalah Diperlukan'
        ]);
        try {
            $student->update($validate);
            return redirect(route('user.index'))->withSuccess('Berjaya Kemaskini');
        } catch (\Throwable $th) {
            return back()->withError('Something when wrong!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
