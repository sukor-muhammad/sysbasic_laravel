<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    public $sortable = ['student_name', 'student_email'];

    protected $table = 'student';
    protected $primaryKey = 'student_id';

    protected $fillable = ['student_name','student_mykad'];


    public function scopeFilter($query, $filters) {
        $query->when($filters['student_name'] ?? false, function($query, $search){
            $query->where('student_name', 'like', "%$search%");
        });
        $query->when($filters['student_mykad'] ?? false, function($query, $search){
            $query->where('student_mykad', 'like', "%$search%");
        });
    }


}
