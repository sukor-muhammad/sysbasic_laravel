<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    public $sortable = ['student_name', 'student_email'];

    protected $table = 'student';
    protected $primaryKey = 'student_id';


    public function scopefilter($query)
    {
        $query->where('active', 1);
    }


}
