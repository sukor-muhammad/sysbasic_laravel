

<form action="{{ route('student.update') }}" method="POST">
    @csrf

    <div class="mb-3">
      <label for="student_name" class="form-label">student_name</label>
      <input value='{{request('student_name')}}' type="text" name='student_name' class="form-control" id="student_name" aria-describedby="emailHelp">
      
    </div>
    <div class="mb-3">
      <label for="student_mykad" class="form-label">student_mykad</label>
      <input value='{{request('student_mykad')}}' type="text" name='student_mykad' class="form-control" id="student_mykad">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

  </form>
