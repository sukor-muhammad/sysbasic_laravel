@extends('layouts.app')


@section('content')


<form>
    <div class="mb-3">
      <label for="student_name" class="form-label">student_name</label>
      <input value='{{request('student_name')}}' type="text" name='student_name' class="form-control" id="student_name" aria-describedby="emailHelp">
      
    </div>
    <div class="mb-3">
      <label for="student_mykad" class="form-label">student_mykad</label>
      <input value='{{request('student_mykad')}}' type="text" name='student_mykad' class="form-control" id="student_mykad">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

  </form>




<table class='table'>
    <tr>
        <th>student name</th>
        <th>student mykad</th>
        <th>action</th>
    </tr>


@foreach ($student as $item)

<tr>
    <td>{{$item->student_name}}</td>
    <td>{{$item->student_mykad}}</td>
    <td>
        
        <a  href='{{route('student.show',$student)}}' type="button" class="btn btn-primary">view</a>
        <a  href='{{}}'  type="button" class="btn btn-info">update</a>
        <a  href='{{}}'  type="button" class="btn btn-danger">delete</a>
    
    </td>
</tr>

    
@endforeach

</table>

{{ $student->appends(request()->except('page'))->links() }}

@endsection


