@extends('layouts.app')


@section('content')


<form>
    <div class="mb-3">
      <label for="student_name" class="form-label">student_name</label>
      <input value='{{request('student_name')}}' type="text" name='student_name' class="form-control" id="student_name" aria-describedby="emailHelp">
      
    </div>
    <div class="mb-3">
      <label for="student_mykad" class="form-label">student_mykad</label>
      <input type="text" name='student_mykad' class="form-control" id="student_mykad">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

  </form>




<table class='table'>
    <th>
        <td>student name</td>
        <td>student mykad</td>
        <td>student email</td>
    </th>


@foreach ($student as $item)

<tr>
    <td>{{$item->student_name}}</td>
    <td>{{$item->student_mykad}}</td>
    <td>{{$item->student_email}}</td>
</tr>

    
@endforeach

</table>

{{ $student->appends(request()->except('page'))->links() }}

@endsection


